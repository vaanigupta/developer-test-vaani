<?php
//File to insert the database data
include 'DB.php'; //include DB connection file
$conn = OpenCon(); // create connection to DB

$startDate = date("Y-m-d"); // today's date
$endDate = date( 'Y-m-d', strtotime( $startDate . ' +365 day' ) ); //date of the day after today's date

//while loop to go through the dates from today until 1 year
while (strtotime($startDate) <= strtotime($endDate)) {
    //generate random 0 and 1 values to populate availabilities
    $value1 = rand(0, 1);
    $value2 = rand(0, 1);
    $value3 = rand(0, 1);

    //sql to insert data into DB
    $sql = "INSERT INTO `databee`.`availability` (`locationId`, `date`, `morning`, `afternoon`, `evening`) VALUES ('1', '{$startDate}', '{$value1}', '{$value2}', '{$value3}')";
    $conn->query($sql);

    $startDate = date ("Y-m-d", strtotime("+1 day", strtotime($startDate)));
}
CloseCon($conn);