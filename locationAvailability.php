<?php
//File to retrieve data from availability table and display it on the location availability page
include 'DB.php'; //includes DB connection file
$conn = OpenCon(); // creates connection to DB

$current_date = date('Y-m-d');; //today's date
$last_date = date('Y-m-d', strtotime( $current_date. ' +3 months' ) ); // last date

$selected_day = $_POST["date"]; // Date passed from the date picker form
$day_before = date( 'Y-m-d', strtotime( $selected_day . ' -1 day' ) ); //One day before the passed date
$day_after = date( 'Y-m-d', strtotime( $selected_day . ' +1 day' ) ); //One day after the passed date

if(strtotime($selected_day) == strtotime($current_date))
{
    //SQL to query the DB for availability of the above three days when the current date is selected
    $sql = "SELECT * FROM databee.availability WHERE date > '{$day_before}' AND date <= '{$day_after}'";
}
else if(strtotime($selected_day) == strtotime($last_date))
{ 
    //SQL to query the DB for availability of the above three days when the last date is selected
    $sql = "SELECT * FROM databee.availability WHERE date >= '{$day_before}' AND date < '{$day_after}'";
}
else {
    //SQL to query the DB for availability of the above three days
	$sql = "SELECT * FROM databee.availability WHERE date >= '{$day_before}' AND date <= '{$day_after}'";
}

$result = $conn->query($sql);

CloseCon($conn);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Location Availability</title>
    <!-- CSS for styling the page-->
    <link rel="stylesheet" href="mystyle.css">
</head>
<body>
<section>
    <h1>Location Availability</h1>
    <table>
        <tr>
            <th>Date</th>
            <th>Morning</th>
            <th>Afternoon</th>
            <th>Evening</th>
        </tr>
        <!-- Using PHP to fetch data from rows-->
        <?php  
        while($row=$result->fetch_assoc())  // Looping till end of the data
        {
            ?>
            <tr>
                <!-- Fetching data from each row of every column-->
                <td><?php echo $row['date'];?></td>
                <td><?php if ($row['morning'] == 1) echo "Available"; elseif ($row['morning'] == 0) echo "Unavailable";?></td>
                <td><?php if ($row['afternoon'] == 1) echo "Available"; elseif ($row['afternoon'] == 0) echo "Unavailable";?></td>
                <td><?php if ($row['evening'] == 1) echo "Available"; elseif ($row['evening'] == 0) echo "Unavailable";?></td>
            </tr>
            <?php
        }
        ?>
    </table>
</section>
</body>
</html>
