<?php
//file to connect to the database
function OpenCon()
{
    //edit these details when testing in your local machine	
    $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $db = "databee";
    $conn = new mysqli($dbhost, $dbuser, $dbpass,$db) or die("Connect failed: %s\n". $conn -> error);

    return $conn;
}

function CloseCon($conn)
{
    $conn -> close();
}

?>