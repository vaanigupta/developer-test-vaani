CREATE TABLE databee.`availability` (
  `availabilityid` int NOT NULL AUTO_INCREMENT,
  `locationId` int NOT NULL,
  `date` date NOT NULL,
  `morning` tinyint(1) NOT NULL,
  `afternoon` tinyint(1) NOT NULL,
  `evening` tinyint(1) NOT NULL,
  PRIMARY KEY (`availabilityid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci