# Databee Full-stack Developer Test - Vaani Gupta #

This README will describe all neccessary steps and requirements to get my code up and running.

### General Description
In this test, I have created a simple webpage "Date Picker" to select a date no more than three months in the future (and not in the past).
The page then displays a three day period-one day either side of the selected date - with a venue's availability for three sessions- Morning, Afternoon, Evening. 
This repository consists of the following files: 

* index.html 
* locationAvailability.php
* DB.php
* dbPopulator.php
* mystyle.css
* script.js
* createTable.sql
* README.md

### Dependencies
* Windows OS
* XAMPP Version: 8.0.3(or higher), XAMPP Control Panel v3.2.4 (Mandatory)
* PHP version- 8.0.3 or higher (Preferred)
* MySQL Workbench 8.0.25 CE (Preferred) 
* Google Chrome (Preferred)

### Execution
(For Windows OS)

* Step 1: Download XAMPP Version: 8.0.3(or higher) for Windows

* Step 2: Run the XAMPP Control Panel v3.2.4 on your local machine

* Step 3: Start the "Apache" Module inside the XAMPP Control Panel

* Step 4: Go to C Drive -> xampp -> htdocs -> create a folder

* Step 5: Download/Pull all files from this repository to this new folder

* Step 6: Inside your browser go to localhost/your folder name/index.html and you will be able to see a date picker form on the webpage

* Step 7: Create a new database schema named "databee" in your local database administration tool (MySQL Workbench 8.0.25 CE preferrable)

* Step 8: In a new SQL tab, execute the SQL query mentioned in "createTable.sql" file (Note: You might need to press the refresh button on the Schemas tab to view the "availability" table)

* Step 9: In "DB.php", change the values of variables "$dbhost, $dbuser, $dbpass" in "OpenCon function" to your connection credentials

* Step 10: Run "dbPopulator.php" i.e. Inside your browser go to localhost/your folder name/dbPopulator.php, to randomly populate data in the "availability" table of the "databee" schema,  you will be able to see a blank webpage

* Step 11: Run "index.html" again in your browser, i.e. localhost/your folder name/index.html 

* Step 12: Select the date you want to check the venue's availability, Click submit

         You will be directed to "locationAvailability.php" and availability of all three sessions will be visible. 
	     Please note you will not be able to see any availability in past or anything after 3 months of the current date.


### Contributer

Vaani Gupta 
vaani@databee.com.au