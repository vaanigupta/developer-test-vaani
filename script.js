// Jquery to restrict users from selecting past dates
$(function(){
    var dtToday = new Date(); //sets current date
    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();

    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();
    var minDate= year + '-' + month + '-' + day; //sets first date to be selected
    document.getElementById("txtDate").setAttribute("min", minDate);
});
// Jquery to restrict users from selecting dates after 3 months
$(function(){
    var dtToday = new Date(); //sets current date
    var month = dtToday.getMonth() + 4;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();

    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();
    var maxDate= year + '-' + month + '-' + day; //sets last date to be selected
    document.getElementById("txtDate").setAttribute("max", maxDate);
 });
